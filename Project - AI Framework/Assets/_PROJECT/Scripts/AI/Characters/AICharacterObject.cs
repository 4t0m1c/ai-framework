using System.Collections.Generic;
using UnityEngine;

public abstract class AICharacterObject : ScriptableObject {

    [Header ("Data")]
    [SerializeField] protected List<AINeedObject> needObjects = new List<AINeedObject> ();
    [SerializeField] protected List<AIStateObject> stateObjects = new List<AIStateObject> ();

    [Header ("Settings")]
    [SerializeField] protected AIStateObject currentState;
    [SerializeField] public AIBehaviour aiBehaviour;

    public void Init (AIBehaviour aiBehaviour) {
        this.aiBehaviour = aiBehaviour;

        for (var i = 0; i < needObjects.Count; i++) {
            needObjects[i] = Instantiate (needObjects[i]);
            needObjects[i].Init (this);
        }

        for (var i = 0; i < stateObjects.Count; i++) {
            if (currentState == stateObjects[i]) {
                stateObjects[i] = Instantiate (stateObjects[i]);
                currentState = stateObjects[i];
            } else {
                stateObjects[i] = Instantiate (stateObjects[i]);
            }
            stateObjects[i].Init (this);
        }
    }

    public AIStateObject GetCurrentState () {
        return currentState;
    }

    public void CheckNeeds () {
        for (var i = 0; i < needObjects.Count; i++) {
            NeedSeverity needSeverity = needObjects[i].CheckNeedStatus ();

            if (needSeverity == NeedSeverity.High) {
                //Do more stuff here
            }
        }
    }

}