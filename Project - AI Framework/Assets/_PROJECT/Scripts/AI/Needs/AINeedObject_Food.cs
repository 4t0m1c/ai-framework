using UnityEngine;

[CreateAssetMenu (fileName = "Need_Food_", menuName = "Data/AI/Needs/Need_Food", order = 0)]
public class AINeedObject_Food : AINeedObject {
    public override NeedSeverity CheckNeedStatus () {
        //Do checks here
        return NeedSeverity.Medium;
    }
}