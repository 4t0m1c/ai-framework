using UnityEngine;

[CreateAssetMenu (fileName = "Need_Water_", menuName = "Data/AI/Needs/Need_Water", order = 0)]
public class AINeedObject_Water : AINeedObject {
    public override NeedSeverity CheckNeedStatus () {
        //Do checks here
        return NeedSeverity.High;
    }
}