using UnityEngine;

[CreateAssetMenu (fileName = "Need_Sleep_", menuName = "Data/AI/Needs/Need_Sleep", order = 0)]
public class AINeedObject_Sleep : AINeedObject {
    public override NeedSeverity CheckNeedStatus () {
        //Do checks here
        return NeedSeverity.Low;
    }
}