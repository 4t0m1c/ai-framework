using UnityEngine;

public abstract class AINeedObject : ScriptableObject {

    protected AICharacterObject characterObject;

    public void Init (AICharacterObject characterObject) {
        this.characterObject = characterObject;
    }

    public abstract NeedSeverity CheckNeedStatus ();

}

public enum NeedSeverity {
    Low,
    Medium,
    High,
    Insane
}