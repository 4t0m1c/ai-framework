using System.Collections;
using UnityEngine;

public abstract class AIStateObject : ScriptableObject {

    protected bool stateEnabled = false;

    protected AICharacterObject characterObject;

    public void Init (AICharacterObject characterObject) {
        this.characterObject = characterObject;
    }

    public virtual void Enable () {
        stateEnabled = true;
    }

    public virtual void Disable () {
        stateEnabled = false;
    }

    protected void StartCoroutine (IEnumerator routine) {
        characterObject.aiBehaviour.StartCoroutine (routine);
    }

}