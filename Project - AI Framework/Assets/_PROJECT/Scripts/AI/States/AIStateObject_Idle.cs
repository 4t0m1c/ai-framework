using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu (fileName = "State_Idle_", menuName = "Data/AI/States/State_Idle", order = 0)]
public class AIStateObject_Idle : AIStateObject {

    NavMeshAgent agent;
    [SerializeField] float checkNeeds = 10;
    [SerializeField] Vector2 idleTime = new Vector2 (1, 10);
    [SerializeField] float wanderRadius = 3;

    public override void Enable () {
        base.Enable ();

        if (agent == null) agent = characterObject.aiBehaviour.agent;

        Debug.Log ($"State_Idle Enabled");
        StartCoroutine (StateLoop ());
    }

    public override void Disable () {
        base.Disable ();
        Debug.Log ($"State_Idle Disabled");
    }

    IEnumerator StateLoop () {
        float randomWait = Random.Range (idleTime.x, idleTime.y);
        Debug.Log ($"Random wait: {randomWait}");
        bool navigating = false;

        while (stateEnabled) {
            if (!navigating) {
                if (randomWait > 0) {
                    randomWait -= Time.deltaTime;
                } else {
                    navigating = true;

                    agent.SetDestination (GetDestination ());
                }
            }
            if (navigating) {
                if (agent.remainingDistance <= agent.stoppingDistance) {
                    Debug.Log ($"You've arrived at your destination.");
                    navigating = false;
                    randomWait = Random.Range (idleTime.x, idleTime.y);
                    Debug.Log ($"Random wait: {randomWait}");

                    characterObject.CheckNeeds ();
                }
            }
            yield return null;
        }
    }

    Vector3 GetDestination () {
        int loopMax = 9999;

        TryAgain:
            loopMax--;
        if (loopMax == 0) {
            Debug.Log ($"Something's not right...");
            return agent.transform.position;
        }

        Vector2 randomCirclePoint = Random.insideUnitCircle * wanderRadius;
        Vector3 randomPoint = agent.transform.position +
            new Vector3 (randomCirclePoint.x, 0, randomCirclePoint.y);

        randomPoint.y += Random.Range (0, 1.5f); //Floors

        Debug.DrawLine (randomPoint, randomPoint + Vector3.up, Color.red, 10);

        if (NavMesh.SamplePosition (randomPoint, out NavMeshHit hit, 0.5f, NavMesh.AllAreas)) {
            return hit.position;
        } else {
            goto TryAgain;
        }
    }

}