using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent (typeof (NavMeshAgent))]
public class AIBehaviour : MonoBehaviour {

    [SerializeField] AICharacterObject characterObject;

    [Header ("References")]
    public NavMeshAgent agent;

    [Header ("Debug")]
    [SerializeField] AIStateObject currentState;

    void OnValidate () {
        if (agent == null) agent = GetComponent<NavMeshAgent> ();
    }

    void Start () {
        characterObject = Instantiate (characterObject);
        characterObject.Init (this);

        ChangeState (characterObject.GetCurrentState ());
    }

    void ChangeState (AIStateObject newState) {
        Debug.Log ($"Changing State {currentState?.name} > {newState.name}");
        currentState?.Disable ();
        currentState = newState;
        currentState?.Enable ();
    }

}